var express = require('express');
var router = express.Router();
var nools = require('nools');

var flow = nools.compile(require.resolve("../../rules/treatment.nools", {name: "treatment"}));
var Patient = flow.getDefined("patient");
var Symptom = flow.getDefined("symptom");
var Question = flow.getDefined("question");

var firedRules = [];
var diagnosisResult, treatmentResult, currentSymptoms;
var currentQuestions = [];
var possibleDiagnosis = [];
var possibleTreatments = [];

router.route('/patient')
    .post(function(req, res) {
      
        var patient = new Patient();
        if (req.body.patient) {
            patient.name = req.body.patient.name;
            patient.age = req.body.patient.age;
            patient.sex = req.body.patient.sex;
            patient.allergies = req.body.patient.allergies;
        }
        
        var symptom = new Symptom();
        if (req.body.symptom) {
            symptom.fever = req.body.symptom.fever == 'true';
            symptom.bodytemp = req.body.symptom.bodytemp;
            symptom.duration = req.body.symptom.duration;
            symptom.soreThroat = req.body.symptom.soreThroat == 'true';
            symptom.headache = req.body.symptom.headache == 'true';
        }
        
        flow.getSession(patient, symptom)
            .on("fire", function (ruleName) {
                firedRules.push(ruleName);
                })
            .on("diagnosis", function (diagnosis) {
                diagnosisResult = diagnosis;
                possibleDiagnosis.push(diagnosis);
            })
            .on("treatment", function (treatment) {
                treatmentResult = treatment;
                possibleTreatments.push(treatment);
            })
            .on("question", function (question) {
                currentQuestions.push(question);
            })
            .on("symptom", function (symptom) {
                currentSymptoms = symptom;
            })
            .match().then(function () {
                res.json({
                    'rulesFired': firedRules,
                    'questions': currentQuestions,
                    'possibleDiagnosis': possibleDiagnosis,
                    'possibleTreatments' : possibleTreatments,
                    'currentSymptoms' : currentSymptoms
                });
                    
                 firedRules = [];
                 currentQuestions = [];
                 possibleDiagnosis = [];
                 possibleTreatments = [];
                 
            }, function (err) {
                console.log(err);
            });
    })
    
    .get(function (req, res) {
        
    });

module.exports = router;