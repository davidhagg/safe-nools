/* global angular */
// public/core.js
var scotchTodo = angular.module('safeNools', []);

function mainController($scope, $http) {
    $scope.formData = {};
        
    // when landing on the page
    // get basic facts and post them
    // $http.post('/api/patient', $scope.formData)
    //     .success(function(data) {
    //         $scope.questions = data.questions;
    //         console.log(data);
    //     })
    //     .error(function(data) {
    //         console.log('Error: ' + data);
    //     });

    // when submitting the add form, send the text to the node API
    $scope.runRules = function() {
        
        $http.post('/api/patient', $scope.formData)
            .success(function(data) {
                //$scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.questions = data.questions;
                $scope.diagnosis = data.possibleDiagnosis;
                $scope.treatments = data.treatments;
                $scope.rulesFired = data.rulesFired;
                
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

    // delete a todo after checking it
    $scope.deleteTodo = function(id) {
        $http.delete('/api/todos/' + id)
            .success(function(data) {
                $scope.todos = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

}